---
title: Creating Curated Communities on Nostr with Ditto
excerpt: 'Ditto empowers users build the community they want to see on Nostr'
coverImage: '/assets/blog/curated-communties/curated-communities-on-nostr.png'
date: '2024-04-28'
author:
  name: Mary Kate Fain
  picture: '/assets/avatars/mk.jpg'
ogImage:
  url: '/assets/blog/curated-communties/curated-communities-on-nostr.png'
---

**For five years I have been building communities for women on the free and decentralized web.** In 2019 I created [Spinster.xyz](https://spinster.xyz/), the largest (and, to my knowledge, the only) women-centered Fediverse server, home to  over 20,000 users. We've faced our fair share of harassment and learned a lot about creating curated and intentional sub-communities on a protocol that is inherently censorship resistent. 

Now, as Soapbox moves it's focus to Nostr, history repeats itself.  Nostr, perhaps even moreso than the Fediverse, is built from the ground up to protect users' free speech. This is a good thing, and it's why we're excited to build on Nostr. But the protocol is young, and without robust tools to curate communities people actually enjoy, Nostr runs the risk of alienating the very users that have the opportunity to bring Nostr (and Bitcoin with it) mainstream.

Ditto provides a solution by allowing users to opt in to curated communities while still remaining connected to the larger Nostr network and enjoying all the benefits of an open protocol. 

## What is Ditto?

**Ditto is a self-hosted server featuring a community-centered Nostr relay with a built-in UI.** 

Each Ditto server hosts it's own community of users, tied together by common identity but free to take their keys and leave at any time. 

Ditto offers robust moderation tools at both the server and user level so that both communities and the individuals within them can craft their ideal experience on Nostr. 

Ditto is still in development, but you can try out the basics today on [Gleasonator.dev](https://gleasonator.dev/) or learn more about Ditto [here](/ditto).

<figure>
  <img alt='Example Ditto Home Page' src='/assets/blog/curated-communties/ditto-server.png' />
  <figcaption>Ditto servers can be customized to each community</figcaption>
</figure>


## Building Community on Nostr with Ditto

**Choose Your Home Server:** Ditto is meant to be self-hosted by multiple independent communties who want to connect together. For example, you may create a Ditto community for a specific geographic region, a special interest like homesteading, or an identity group like women. Ditto servers can be customized with colors, branding, and more to help your community feel unique. Users can take their existing npub to any Ditto server and opt in to making it their home server, and change home servers at any time! Users always retain control over their keys. 

**NIP-05 Identities:** Ditto utilitizes NIP-05 identities (usernames granted by a specific domain) to mark users as part of the community. Ditto features a built-in self-service name provider for users, which admins can approve or revoke. This is how Ditto admins will determine who is part of their community, and how users will indicate their choice of home server.

<figure>
  <img alt='Set your identity on Ditto' src='/assets/blog/curated-communties/set-identity.png' />
  <figcaption>Choose how to be identified on your home Ditto server</figcaption>
</figure>

**Local Feed:** The local feed is a homebase of each Ditto server, where users who are identified on the server can see and engage with each-other's posts. 

<figure>
  <img alt='Example Ditto Local Feed' src='/assets/blog/curated-communties/local-feed.png' />
  <figcaption>Anyone with a NIP-05 on the server's domain shows in the Local Feed</figcaption>
</figure>


## Server-level Moderation Options on Ditto

Each Ditto server can control the experience on Nostr for its community through server-level moderation. There are multiple ways to do this, providing a variety of options for the needs of each community. 

**Muted Word Policies:** Mute any notes with specific words for the whole server.

**Server-level User Muting:** Mute individual npub's for your whole server so individual users don't have to bother with obvious bad actors.

**Domain Muting:** An entire other Ditto serving acting badly? Mute the whole server. 

**AI Content Policies:** Connect your server to an OpenAI scoring tool with specific rules to approve individual posts.

*Don't like how your Home Server is moderating content? Too much censorship? Not enough? You can take your keys to any other Nostr client, including Ditto servers, to get the experience you want! Or, better yet, host your own Ditto server and take control of moderation yourself!*


## User-level Moderation Options on Ditto

Users can further curate their own feeds by muting specific other users, including users on their own home server. Future releases of Ditto will also alow users to mute specific words, entire conversations, and other domains.

---

## Join the First Woman-Only Ditto server

Ditto is still in development, but you can sign up now for early access to the first community Ditto server: henhouse.social, a Nostr relay just for women! Learn more and apply to join:

<Button theme='secondary' href='https://henhouse.social'>Sign Up For Early Access to Hen House</Button>

