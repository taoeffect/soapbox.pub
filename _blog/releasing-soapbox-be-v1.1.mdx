---
title: Releasing Soapbox BE v1.1
excerpt: The Erlang 24 cataclysm event. I hate to start off with something negative, but it needs to be addressed. For a long time we pointed to Pleroma's source install guide, and even used it as the starting point for our own install docs. In that guide, we recommended installing a repo from "erlang-solutions.com" on your server. This was a mistake, as that repo pushes out bleeding edge versions of Erlang literally on the day of a new release.
coverImage: '/assets/blog/releasing-soapbox-be-v1.1/be-1-1-thumb.png'
date: '2021-05-18'
author:
  name: Alex Gleason
  picture: '/assets/avatars/alex.png'
ogImage:
  url: '/assets/blog/releasing-soapbox-be-v1.1/be-1-1-thumb.png'
---

## The Erlang 24 cataclysm event

I hate to start off with something negative, but it needs to be addressed.

For a long time we pointed to Pleroma's [source install guide](https://docs.pleroma.social/backend/installation/debian_based_en/), and even used it as the starting point for our own install docs.

In that guide, we recommended installing a repo from "erlang-solutions.com" on your server.
This was a mistake, as that repo pushes out bleeding edge versions of Erlang literally on the day of a new release.
As a result, Pleroma installations (and Soapbox BE) installed this way stopped functioning when Erlang 24 was released, and after running `apt upgrade` on the machine.

It is normal for software to lag a bit behind major language releases.
We are working on updating Soapbox BE to be Erlang 24 compatible.
What's not normal is deploying a repo with breaking changes in a production setting.

Our install guide has been updated to recommend using [asdf version manager](https://asdf-vm.com/) instead.
This will allow us to specify the Erlang/Elixir version for a release, while the server admin need only run `asdf install` to get the right versions.

## Upgrading to Soapbox BE 1.1 (with asdf)

If you already have Soapbox BE 1.0 or a vanilla Pleroma 2.3 server, you can follow these steps to update your system to Soapbox BE 1.1.
This guide assumes you're using Ubuntu 20.04 LTS.

First, install new OS requirements:

```sh
apt install imagemagick ffmpeg libimage-exiftool-perl unzip libssl-dev automake autoconf libncurses5-dev
```

Next, become the pleroma user and switch to the latest Soapbox:

```sh
cd /opt/pleroma
sudo -Hu pleroma bash
git remote set-url origin https://gitlab.com/soapbox-pub/soapbox.git
git fetch origin --tags
git checkout soapbox-v1.1.1
```

Next, install asdf, Elixir 1.11, and Erlang 23:

```sh
git clone https://github.com/asdf-vm/asdf.git ~/.asdf --branch v0.8.0
echo ". $HOME/.asdf/asdf.sh" >> ~/.bashrc
echo ". $HOME/.asdf/completions/asdf.bash" >> ~/.bashrc
exec bash
asdf plugin-add erlang
asdf plugin-add elixir
asdf install
```

(Erlang gets built from source, so this will take about 15 minutes ☕)

Recompile Pleroma:

```sh
mix local.hex --force
mix local.rebar --force
mix deps.get
MIX_ENV=prod mix compile
```

(This will take about 10 minutes ☕)

If you started with a Pleroma version prior to 2.3, you need to also update the database:

```sh
MIX_ENV=prod mix ecto.migrate
```

Next, become root:

```sh
exit
```

Finally, as root, copy the updated systemd file, and restart:

```sh
cp /opt/pleroma/installation/pleroma.service /etc/systemd/system/pleroma.service
systemctl daemon-reload
systemctl restart pleroma
```

At this point Soapbox BE will be using Erlang/Elixir installed from asdf, so you can now safely `apt upgrade` without breaking your system.

If you have a Pleroma server and do not want to switch to Soapbox BE, you can follow [these instructions](https://gitlab.com/-/snippets/2121092) on switching to asdf instead.

In the future, upgrading Soapbox BE will be a lot easier.
Thanks for your patience!

## Unletterboxed media

![Unletterboxed media screenshot](/assets/blog/releasing-soapbox-be-v1.1/attachment-dimensions.png)

Uploaded attachments are now inspected for their length/width in pixels, which lets us render them beautifully, unletterboxed.

We still don't process remote media, though (yet).
We send our own attachment data out through ActivityPub, and receive incoming attachment data.
As a result, Soapbox to Soapbox federation renders images unletterboxed.

If you previously configured upload filters, you'll need to ensure that `SetMeta` is enabled in AdminFE under "Uploads > Filters":

![Upload filters screenshot](/assets/blog/releasing-soapbox-be-v1.1/upload-filters.png)

(We recommend `Exiftool` be enabled for security reasons as well, as it strips GPS data from uploads. Click the trash icon to return to the defaults.)

## Blurhash support

<video src="/assets/blog/releasing-soapbox-be-v1.1/blurhash.mp4" autoPlay loop muted></video>

Uploaded images now have blurhash support, which makes them look nicer while loading, and improves the look of images with content warnings.

The same rules as unletterboxed media apply: the `SetMeta` upload filter must be enabled, and blurhash only federates between supported servers (including Mastodon).

## Bug fixes

- Rich media not working for certain links. ([!19](https://gitlab.com/soapbox-pub/soapbox/-/merge_requests/19), [!20](https://gitlab.com/soapbox-pub/soapbox/-/merge_requests/20))

## Installing Soapbox

To install a fresh server, see our [install guide](/install/).

## Update: 1.1.1 (May 21, 2021)
A new minor release fixes the following bugs:

- Broken frontend due to not all files being included.

## Fund the Soapbox Project

Soapbox is funded entirely by donations. If you would like to support me, you can send me a donation. Your support is greatly appreciated, and every bit counts.

- Credit Card — [PayPal](https://paypal.me/gleasonator) (alex@alexgleason.me)
- Bitcoin — `bc1q9cx35adpm73aq2fw40ye6ts8hfxqzjr5unwg0n`
- Etherium — `0xAc9aB5Fc04Dc1cB1789Af75b523Bd23C70B2D717`
- Dogecoin — `D5zVZs6jrRakaPVGiErkQiHt9sayzm6V5D`
- Ubiq (10grans) — `0x541a45cb212b57f41393427fb15335fc89c35851`
- Monero — `45JDCLrjJ4bgVUSbbs2yjy9m5Mf4VLPW8fG7jw9sq5u69rXZZopQogZNeyYkMBnXpkaip4p4QwaaJNhdTotPa9g44DBCzdK`

## Thank you!

A huge thank you for everyone's patience as we resolve these difficult challenges.
Social media is hard, and computers are harder.
I'm glad this project is more resilient now.