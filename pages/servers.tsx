import { IconExternalLink } from '@tabler/icons';

import Container from '../components/container';
import FormattedNumber from '../components/formatted-number';
import Layout from '../components/layout';
import PageTitle from '../components/page-title';
import UnifiedMeta from '../components/unified-meta';
import { fetchServers, Server } from '../lib/servers';

interface IServersPage {
  servers: Server[]
}

export default function ServersPage({ servers }: IServersPage) {
  return (
    <Layout>
      <UnifiedMeta
        title='Servers | Soapbox'
      />
      <Container>
        <div className='max-w-4xl mx-auto'>
          <PageTitle className='my-16'>
            Servers
          </PageTitle>

          <div className='grid md:grid-cols-3 gap-20 mb-32'>
            {servers.map(server => (
              <div key={server.domain} className='space-y-3'>
                <div>
                  <div className='text-xl'>{server.title}</div>
                  <div><span className='font-bold'><FormattedNumber number={server.userCount} /></span> users</div>
                </div>

                <div>{server.description}</div>

                <a
                  href={`https://${server.domain}`}
                  className='flex space-x-1 items-center text-azure font-bold'
                  target='_blank'
                >
                  <span>Join</span>
                  <IconExternalLink height={20} />
                </a>
              </div>
            ))}
          </div>
        </div>
      </Container>
    </Layout>
  );
}

export const getStaticProps = async () => {
  const servers = await fetchServers();

  return {
    props: { servers },
  };
};