import Button from '../components/button';
import Container from '../components/container';
import Layout from '../components/layout';
import PageTitle from '../components/page-title';
import TeamMember from '../components/team-member';
import UnifiedMeta from '../components/unified-meta';

export default function DittoPage() {
  return (
    <Layout>
      <UnifiedMeta title='Ditto | Soapbox' />

      <div className='bg-gradient-to-r from-fuchsia-500 to-violet-500 text-white p-8 lg:p-20'>
        <div className='grid lg:grid-cols-2 gap-8 lg:gap-16 container mx-auto'>

          <img className='rounded-2xl max-w-full shadow' src='/assets/images/ditto-screenshot.png' />

          <div>
            <div className='max-w-prose space-y-8'>
              <PageTitle>
                <span className='text-white'>Ditto: Coming Soon!</span>
              </PageTitle>

              <div className='space-y-4'>
                <h2 className='text-2xl font-semibold leading-snug'>
                  ActivityPub + Nostr = Decentralized Future
                </h2>
  
                <p className='text-xl'>Ditto is a decentralized, self-hosted social media server that emphasizes user control and community building across platforms. Key features include a built-in Nostr relay, compatibility with any Mastodon app, and full customizability. Always open-source, no ads, no tracking, and no censorship. </p>
              </div>

              <Button theme='secondary' href='https://docs.soapbox.pub/ditto/'>Read the Docs</Button>
            </div>
          </div>
        </div>
      </div>

      <Container className='flex justify-center my-24'>
        <div className='flex flex-col items-center space-y-8 text-center rounded-full bg-white shadow-white shadow-[0_20px_50px_50px]'>
          <h2 className='text-3xl lg:text-5xl font-semibold leading-none'>
            Related Projects
          </h2>
          <h2 className='text-xl font-semibold mt-6 mb-24'>
            Building an flexible ecosystem with tools for use across platforms
          </h2>

          <div className='grid lg:grid-cols-3 gap-20'>
            <TeamMember
              name='Mostr Bridge'
              avatar='/assets/images/ditto-cartoon-planet.png'
              bio='Mostr is a bridge between Nostr and the Fediverse (Mastodon, ActivityPub, etc). It allows users on both networks to communicate, through a Mostr server.'
              url='https://soapbox.pub/blog/mostr-fediverse-nostr-bridge/'
            />

            <TeamMember
              name='Nostrify'
              avatar='/assets/images/ditto-cartoon-ufo.png'
              bio='Bring your projects to life on Nostr with this flexible and pure Typscript framework on Deno and web.'
              url='https://nostrify.dev/'
            />

            <TeamMember
              name='Strfry Policies'
              avatar='/assets/images/ditto-cartoon-telescope.png'
              bio='A collection of policies for the strfry Nostr relay, built in Deno.'
              url='https://gitlab.com/soapbox-pub/strfry-policies'
            />
          </div>
        </div>
      </Container>

      <div className='bg-gradient-to-r from-indigo-500 to-blue-500 text-white py-20'>
        <Container>
          <div className='max-w-prose space-y-6'>
            <h2 className='text-4xl font-semibold'>Get Involved</h2>
            <p className='text-xl'>Soapbox is seeking active contributors to join our contributor community. Browse open issues on GitLab to begin contributing today!</p>
            <div className='space-x-4'>
              <Button theme='secondary' href='https://gitlab.com/soapbox-pub/ditto'>Contribute</Button>
            </div>
            <p>Looking for work? We're also looking for core contributors open to contract. <br />Get in touch if that could be you!</p>
          </div>
        </Container>
      </div>

    </Layout>
  );
}