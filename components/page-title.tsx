import clsx from 'clsx';

interface IPageTitle {
  className?: string
  children: React.ReactNode
}

const PageTitle: React.FC<IPageTitle> = ({ className, children }) => {
  return (
    <h1 className={clsx(className, 'text-6xl text-azure font-bold leading-snug')}>
      {children}
    </h1>
  );
};

export default PageTitle;