import clsx from 'clsx';

interface IWrapper {
  children: React.ReactNode
  className?: string
}

const Wrapper: React.FC<IWrapper> = ({ children, className }) => {
  return (
    <div className={clsx(className, 'max-w-2xl mx-auto')}>
      {children}
    </div>
  );
};

export default Wrapper;