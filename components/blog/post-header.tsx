import DateFormatter from '../date-formatter';
import Wrapper from '../wrapper';

import Author from './author';
import CoverImage from './cover-image';
import PostTitle from './post-title';

import type { Author as AuthorType } from 'contentlayer/generated';

type Props = {
  title: string
  coverImage: string
  date: string
  author: AuthorType
}

const PostHeader = ({ title, coverImage, date, author }: Props) => {
  return (
    <>
      <PostTitle>{title}</PostTitle>
      <div className='hidden md:block md:mb-12'>
        <Author name={author.name} picture={author.picture} />
      </div>
      <div className='mb-8 md:mb-16 sm:mx-0'>
        <CoverImage title={title} src={coverImage} />
      </div>
      <Wrapper>
        <div className='block md:hidden mb-6'>
          <Author name={author.name} picture={author.picture} />
        </div>
        <div className='mb-6 text-lg text-gray-500'>
          <DateFormatter dateString={date} />
        </div>
      </Wrapper>
    </>
  );
};

export default PostHeader;
