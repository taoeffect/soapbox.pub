import clsx from 'clsx';

interface IFeatures {
  children: React.ReactNode
}

const Features: React.FC<IFeatures> = ({ children }) => {
  return (
    <div className='grid grid-cols-1 lg:grid-cols-3 gap-4'>
      {children}
    </div>
  );
};

interface IFeature {
  children: React.ReactNode
  className?: string
  span?: 1 | 2 | 3
  direction?: 'vertical' | 'horizontal'
  title?: React.ReactNode
  blurb?: React.ReactNode
}

const Feature: React.FC<IFeature> = ({
  children,
  className,
  span = 1,
  direction = 'vertical',
  title,
  blurb,
}) => {
  return (
    <div className={clsx(
      className,
      'relative rounded-xl bg-white overflow-hidden shadow drop-shadow-md flex flex-col',
      {
        'lg:col-span-1': span === 1,
        'lg:col-span-2': span === 2,
        'lg:col-span-3': span === 3,
      },
    )}>
      <div className={clsx({ 'lg:flex space-x-6 h-full': direction === 'horizontal' })}>
        {(title || blurb) && (
          <div className='p-6 space-y-4'>
            {title && (
              <FeatureTitle>{title}</FeatureTitle>
            )}
            {blurb && (
              <p>{blurb}</p>
            )}
          </div>
        )}

        {children}
      </div>
    </div>
  );
};

interface IFeatureTitle {
  children: React.ReactNode
}

const FeatureTitle: React.FC<IFeatureTitle> = ({ children }) => {
  return (
    <h2 className='text-2xl lg:text-4xl'>
      {children}
    </h2>
  );
};

export {
  Features,
  Feature,
  FeatureTitle,
};