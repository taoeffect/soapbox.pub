import Link from 'next/link';

type AnchorProps = React.DetailedHTMLProps<React.AnchorHTMLAttributes<HTMLAnchorElement>, HTMLAnchorElement>;

interface ISmartLink extends Omit<AnchorProps, 'href' | 'ref'> {
  href: string
}

/** Routes internal links with Next.js routing, and opens external links in a new tab. */
const SmartLink: React.FC<ISmartLink> = (props) => {
  if (props.href?.startsWith('/')) {
    return <Link {...props} />;
  } else {
    return <a target='_blank' {...props} />;
  }
};

export default SmartLink;