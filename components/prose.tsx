import markdownStyles from './markdown-styles.module.css';

interface IProse extends Pick<React.HTMLAttributes<HTMLDivElement>, 'dangerouslySetInnerHTML'> {
  children?: React.ReactNode
}

const Prose: React.FC<IProse> = ({ children, dangerouslySetInnerHTML }) => {
  return (
    <div
      className={markdownStyles['markdown']}
      dangerouslySetInnerHTML={dangerouslySetInnerHTML}
    >
      {children}
    </div>
  );
};

export default Prose;