import Avatar from './avatar';

interface ITeamMember {
  name: string
  avatar: string
  bio: string
  url?: string
}

const TeamMember: React.FC<ITeamMember> = ({ name, avatar, bio, url }) => {
  return (
    <div className='flex flex-col items-center gap-6'>
      <a href={url} target='_blank'>
        <Avatar src={avatar} size={256} alt={name} />
      </a>
      <h3 className='text-2xl font-semibold'>{name}</h3>
      <p>{bio}</p>
    </div>
  );
};

export default TeamMember;