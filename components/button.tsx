import clsx from 'clsx';

import SmartLink from './smart-link';

interface IButton {
  href: string
  theme: 'secondary' | 'outline'
  children: React.ReactNode
  className?: string
  group?: boolean
}

const Button: React.FC<IButton> = ({ href, theme, children, className, group }) => {
  return (
    <SmartLink
      href={href}
      className={clsx(
        className,
        'text-lg py-4 px-8 rounded-full',
        'inline-block focus:outline-none focus:ring-2 focus:ring-offset-2 appearance-none transition-all',
        'no-underline decoration-transparent',
        {
          'group': group,
          'bg-teal': theme === 'secondary',
          'border-2 border-white': theme === 'outline',
        },
      )}
    >
      <span
        className={clsx('no-underline', {
          'text-white': theme === 'secondary',
        })}
      >
        {children}
      </span>
    </SmartLink>
  );
};

export default Button;