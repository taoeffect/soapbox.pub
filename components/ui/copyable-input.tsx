import React, { useRef } from 'react';
import { FormattedMessage } from 'react-intl';

import HStack from './hstack';
import Input from './input';

interface ICopyableInput {
  /** Text to be copied. */
  value: string,
}

/** An input with copy abilities. */
const CopyableInput: React.FC<ICopyableInput> = ({ value }) => {
  const input = useRef<HTMLInputElement>(null);

  const selectInput = () => {
    input.current?.select();

    if (navigator.clipboard) {
      navigator.clipboard.writeText(value);
    } else {
      document.execCommand('copy');
    }
  };

  return (
    <HStack alignItems='center'>
      <Input
        ref={input}
        type='text'
        value={value}
        className='rounded-r-none rtl:rounded-l-none rtl:rounded-r-lg'
        outerClassName='flex-grow'
        onClick={selectInput}
        readOnly
      />

      <button
        className='space-x-2 rtl:space-x-reverse inline-flex items-center border font-medium rounded-full focus:outline-none focus:ring-2 focus:ring-offset-2 appearance-none transition-all bg-azure border-transparent text-gray-100 focus:ring-azure px-4 py-2 text-sm mt-1 h-full rounded-l-none rounded-r-lg rtl:rounded-l-lg rtl:rounded-r-none'
        onClick={selectInput}
      >
        <FormattedMessage id='input.copy' defaultMessage='Copy' />
      </button>
    </HStack>
  );
};

export default CopyableInput;
