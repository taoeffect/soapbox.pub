import { IconMail } from '@tabler/icons';
import clsx from 'clsx';
import { useState } from 'react';
import { useMailChimpForm } from 'use-mailchimp-form';

import Input from './ui/input';
import Stack from './ui/stack';

const MAILCHIMP_URL = 'https://host.us7.list-manage.com/subscribe/post?u=f1f718a93795f42a604d558d8&amp;id=f7271442a0&amp;f_id=00ead6e4f0';

const MAILCHIMP_TAGS = {
  Ditto: '7339801',
};

interface IEmailSignupForm {
  url?: string
  tags?: Array<keyof typeof MAILCHIMP_TAGS>
}

const EmailSignupForm: React.FC<IEmailSignupForm> = ({ url = MAILCHIMP_URL, tags }) => {
  const [email, setEmail] = useState('');

  const {
    loading,
    error,
    success,
    message,
    handleSubmit,
  } = useMailChimpForm(url);

  const disabled = loading || success;

  const onSubmit: React.FormEventHandler<HTMLFormElement> = (e) => {
    e.preventDefault();
    handleSubmit({
      EMAIL: email,
      tags: tags?.map(tag => MAILCHIMP_TAGS[tag])?.join(','),
    });
  };

  return (
    <form onSubmit={onSubmit}>
      <fieldset disabled={disabled}>
        <Stack space={2} alignItems='start'>
          <Input
            outerClassName='w-full'
            className='!text-lg'
            type='email'
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            placeholder='Your email address'
            prepend={<IconMail className='ml-3 text-gray-400' size={24} />}
            append={(
              <button
                type='submit'
                className={clsx('bg-teal text-white rounded-md px-4 py-2 -mr-2 text-base font-medium', {
                  'hover:bg-opacity-90 focus:outline-none focus-visible:ring-2 focus-visible:ring-white focus-visible:ring-opacity-75': !disabled,
                })}
                disabled={disabled}
              >
                Sign up
              </button>
            )}
            disabled={disabled}
            required
          />
          {(error && message) && (
            <div className='text-red-500 text-sm'>
              {message.replace('0 - ', '')}
            </div>
          )}
          {(success && message) && (
            <div className='text-green-500 text-sm'>
              {message.replace('0 - ', '')}
            </div>
          )}
        </Stack>
      </fieldset>
  </form>
  );
};

export default EmailSignupForm;